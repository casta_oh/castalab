// config/passport.js

// load all the things we need
var LocalStrategy   = require('passport-local').Strategy;

var userMongo = require('../models/userMongo');

// expose this function to our app using module.exports
module.exports = function(passport) {

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        console.log('Serializando el usuario: ' + user.id);
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        console.log('Desearializando el usuario: ' + id);
        userMongo.findById(id, function(err, user) {

            console.log('He encontrado el usuario?: ' + user);

            done(err, user);
        });
    });

    passport.use('local-login', new LocalStrategy({
    	usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    	function(req, username, password, callback) {

    		console.log('Vamos a hacer login con estrategia local para usuario: ' + username);
    	
    		userMongo.findOne({ 'username' :  username }, function(err, user) {

		        console.log('Se ha encontrado el usuario: ' + user);

				if (err)
		          return callback(err);
		        
		        if (!user){
		          console.log('User Not Found with username '+username);
		          return callback(null, false);                 
		        }
		        
		        if (!user.validPassword(password))
                	return callback(null, false);

            	// all is well, return successful user
            	console.log('Login realizado correctamente');
            	return callback(null, user);   	
                return callback(null, user);


			});
    	}
    ));

};