var express = require('express');
var router = express.Router();
var gcmuserMongo = require('../models/gcmuserMongo.js')
var authController = require('../controllers/auth_controller');
var mongoose = require('mongoose/');

router.get('/', isLoggedIn, function(req, res) {
  
	console.log('Obteniendo todos los usuarios de gcm');
	
  gcmuserMongo.find({}, function(err, users) {
    console.log(err);
		if(err)
            res.send('Errores obteniendo los usuarios', 500);
        else
            res.json(users);

	});

});

router.put('/', isLoggedIn, function(req, res) {
  
  console.log('Insertando el gcm_regid: ' + JSON.stringify(req.body));
  
  var nuevoUsuario = {'gcmUser':req.body.gcmUser };

  mongoose.connection.db.collection('gcmUser').insert(nuevoUsuario, function(err, rows) {

    console.log('Ha habido errores insertando la coleccion?: ' + err);
    if(err)
        res.send('Errores insertando el usuario: ' + req.body, 500);
    else
        res.send('Usuario insertado');

  });   

  
});

router.delete('/:id', isLoggedIn, function(req, res) {
  
  console.log('Borrando el usuario ' + req.params.id);


  mongoose.connection.db.collection('gcmUser').remove({ 'gcm_regid' :  req.params.id }, function(err, rows) {

    console.log('Ha habido errores borrando la coleccion?: ' + err);
    
    if(err)
        res.send('Errores insertando el usuario: ' + req.params.id, 500);
    else
        res.send('Usuario insertado');

  });  

});

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.send('No se ha hecho login en la aplicacion', 500);
}

module.exports = router;
