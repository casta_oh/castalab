module.exports = function(app, passport) {

    app.post('/login', function(req, res, next) {
          passport.authenticate('local-login', function(err, user, info) {
            if (err) {
              return res.status(500).json({err: err});
            }
            if (!user) {
              return res.status(401).json({err: info});
            }
            req.logIn(user, function(err) {
              if (err) {
                return res.status(500).json({err: 'Could not log in user'});
              }
              res.status(200).json({status: 'Login successful!'});
            });
          })(req, res, next);
    });

    app.get('/logout', function(req, res) {
      req.logout();
      res.status(200).json({status: 'Bye!'});
    });


}; 