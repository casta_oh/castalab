var express = require('express');
var router = express.Router();
var gcmMap = require('../models/gcmMap.js')
var authController = require('../controllers/auth_controller');
var mongoose = require('mongoose/');

router.get('/', isLoggedIn, function(req, res) {
  
	console.log('Obteniendo el ultimo mapa json');

  gcmMap.find({}, function (err, maps) {
    console.log('Ha habido algun error?: ' + err);
		if(err)
            res.send('Errores obteniendo el ultimo mapa', 500);
        else
            res.json(maps[0].gcmMap);

	});

});

router.put('/', isLoggedIn, function(req, res) {
  
  console.log('Insertando el nuevo mapa json: ' + JSON.stringify(req.body));
  
  var newMap = {'gcmMap': req.body };

  mongoose.connection.db.collection('gcmMap').remove();
  mongoose.connection.db.collection('gcmMap').insert(newMap, function(err, rows) {

    console.log('Ha habido errores insertando el mapa?: ' + err);
    if(err)
        res.send('Errores insertando el mapa: ' + JSON.stringify(req.body), 500);
    else
        res.json(newMap);
  });   
  
});

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    console.log('Comprobando si he accedido para mapas: ' + req.isAuthenticated());

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.send('No se ha hecho login en la aplicacion', 500);
}

module.exports = router;