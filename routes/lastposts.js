var express = require('express');
var router = express.Router();
var lastPost = require('../models/lastPost.js')
var authController = require('../controllers/auth_controller');
var mongoose = require('mongoose/');

router.get('/', isLoggedIn, function(req, res) {
  
	console.log('Obteniendo el ultimo post');

  lastPost.find({}, function (err, posts) {
    console.log(posts[0].lastPost);
    console.log('Ha habido algun error?: ' + err);
		if(err)
            res.send('Errores obteniendo el ultimo post', 500);
        else
            res.json(posts);

	});

});

router.put('/', isLoggedIn, function(req, res) {
  
  console.log('Insertando el ultimo articulo json: ' + JSON.stringify(req.body));
  
  var newLastPost = {'lastPost': req.body };

  mongoose.connection.db.collection('lastPost').remove();
  mongoose.connection.db.collection('lastPost').insert(newLastPost, function(err, rows) {

    console.log('Ha habido errores insertando el ultimo post?: ' + err);
    if(err)
        res.send('Errores insertando el ultimo post: ' + JSON.stringify(req.body), 500);
    else
        res.json(newLastPost);
  });   
  
});

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    console.log('Comprobando si he accedido para ultimo articulo: ' + req.isAuthenticated());
    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.send('No se ha hecho login en la aplicacion', 500);
}

module.exports = router;