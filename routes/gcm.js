//REG_ID DE PRUEBA: 
//APA91bGFctInjP9DFDfV5P5br_UiZyhpWBvF4qO8HzMMFZpym9QI3-EdGVf8GJ7zGX7f38hsP6KSBJTezadaFc_nskavhfS6UwdZSAWIwsKs7ap3POOLctNekyx7JiBl7WdbewXv0j5W
//APA91bFr0wzcQ16CI1Hzwq8I33mfVLcHy7itPxA30WKWhevOlx4vv6o_0OmCRn7y6uaX47qLK6WhjjzYfnW7w0HvZF0KL58MKtPDh2qJTlRhAVcH3mklROTBBrsTxmV5ROcbS44Squgv
//APA91bEfQ_l7nJwnt-Rdcw2Ug3SiFxwyHFBAX-x6I7oemLWUbBn_JCi62VYdaqIPCbDPVBadoAsTnxYoLszqv_r6MFuZLB7hFcFixAxzmABxjnI5pKOiqAqbtkD-QfPoKDbKcE80Sp22
var express = require('express');
var router = express.Router();
var authController = require('../controllers/auth_controller');
var gcm = require('node-gcm');
var gcmuserMongo = require('../models/gcmuserMongo.js')


router.post('/', isLoggedIn, function(req, res) {
	
  	console.log('Enviando la notificacion PUSH a los usuarios: ' + JSON.stringify(req.body));

	//obteniendo todos los usuarios de mongo
	
	gcmuserMongo.find({}, function(err, users) {

    	console.log("Ha habido errores obteniendo los usuarios de mongo?: " + err);
		if(err)
            res.send('Errores obteniendo los usuarios de mongo', 500);
        else
    		
    		console.log("Respuesta: " + JSON.stringify(users));
			//preparando el mensaje a enviar
			var message = new gcm.Message();
			message.addData('message',req.body.mensaje);

			var regTokens = [];
			var arrayLength = users.length;
			
			console.log("Numero de usuarios obtenidos: " + arrayLength);
			for (var i = 0; i < arrayLength; i++) {
				console.log("gcmUser: " + JSON.stringify(users[i]));
				regTokens.push(users[i].gcmUser);
			}

			console.log("Peticiones a GCM: " + regTokens);


        	// Set up the sender with you API key
			var sender = new gcm.Sender('AIzaSyBpiSYbj6IqmqGiIAkTLxQPPcHK6-U_Y18');

			// Now the sender can be used to send messages
			sender.send(message, { registrationTokens: regTokens }, function (err, response) {
			    
				console.log("Respuesta: " + JSON.stringify(response));
				console.log("Error: " + err);

			    if(err) 
			    	res.send(err);
			    else    
			    	res.send(response);

			});

	});

	

});

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.send('No se ha hecho login en la aplicacion', 500);
}

module.exports = router;