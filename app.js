//integracion con GCM https://www.npmjs.com/package/node-gcm
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');

var routes = require('./routes/index');
var gcmusers = require('./routes/gcmusers');
var gcm = require('./routes/gcm');
var maps = require('./routes/maps');
var lastPosts = require('./routes/lastposts');

var passport = require('passport');

var mongoose = require('mongoose/');
var flash    = require('connect-flash');


var app = express();

//conectamos a mongoose
var database = require('./config/database.js');
mongoose.connect(database.url);


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(logger('dev'));

app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Use the passport package in our application
app.use(session({ secret: 'keyboard' }));
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions

app.use(flash()); // use connect-flash for flash messages stored in session
 
require('./config/passport')(passport);

require('./routes/login.js')(app, passport); // load our routes and pass in our app and fully configured passport

app.use('/api/gcmusers', gcmusers);
app.use('/api/sendpushnotification', gcm);
app.use('/api/maps', maps);
app.use('/api/lastpost', lastPosts);

 app.get('*', function(req, res) {
        res.sendfile('./public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
    });

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});



module.exports = app;
