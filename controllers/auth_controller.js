//DEPRECATED
// Load required packages
var passport = require('passport');
var BasicStrategy = require('passport-http').BasicStrategy;
var bcrypt = require('bcrypt-nodejs');

var UserMongo = require('../models/userMongo');

var isValidPassword = function(user, password){
  return bcrypt.compareSync(password, user.password);
}

passport.use(new BasicStrategy( 
  function(username, password, callback) {
    
    //var tmpPassword = bcrypt.hashSync(password, bcrypt.genSaltSync(10), null);
    //console.log("la contraseña encriptada es: " + tmpPassword);

    console.log('Vamos a hacer login con estrategia basica para usuario: ' + username);
    
    UserMongo.findOne({ 'username' :  username }, 
      function(err, user) {

        console.log('Se ha encontrado linea: ' + user.username);


        // In case of any error, return using the done method
        if (err)
          return callback(err);
        // Username does not exist, log error & redirect back
        
        if (!user){
          console.log('User Not Found with username '+username);
          return done(null, false, 
                req.flash('message', 'User Not found.'));                 
        }
        
        // User exists but wrong password, log the error 
        if (!isValidPassword(user, password)){
          
          console.log('Usuario y contrasenia incorrectos');
          return callback(null, false, 
              req.flash('message', 'Invalid Password'));
        }
        console.log('Login realizado correctamente');
        return callback(null, user);
      }
    );

  }
));

exports.isAuthenticated = passport.authenticate('basic', { session : false });