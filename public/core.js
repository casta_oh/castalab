'use strict';

var castaLabApp = angular.module('castaLabApp', [
  'ngRoute',
  'castaLabControllers',
  'castaLabServices'
  ]);

castaLabApp.run(function ($rootScope, $location, $route, AuthService) {

  $rootScope.$on('$routeChangeStart', function (event, next, current) {
    if (next.access.restricted && AuthService.isLoggedIn() === false) {
      $location.path('/public/login');
      $route.reload();
    }
  });

});

castaLabApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/public/lastpost', {
            templateUrl: 'partials/lastpost.html',
            controller: 'LastPostController',
            access: {restricted: true }
      }).
      when('/public/lastmap', {
            templateUrl: 'partials/lastmap.html',
            controller: 'LastMapController',
            access: {restricted: true }
      }).
      when('/public/gcmusers', {
            templateUrl: 'partials/gcmusers.html',
            controller: 'GCMUsersController',
            access: {restricted: true }
      }).
      when('/public/sendpushnotification', {
            templateUrl: 'partials/sendpushnotification.html',
            controller: 'SendPushNotificationController',
            access: {restricted: true }
      }).
      when('/public/login', {
        templateUrl: 'partials/login.html', 
        controller: 'LoginController',
        access: {restricted: false }
      }).
      when('/public/logout', {
        controller: 'LogoutController',
        access: {restricted: true }
      }).
      otherwise({
        redirectTo: '/public/login',
        access: {restricted: false }
      });
  }]);