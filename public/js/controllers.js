'use strict';

var castaLabControllers = angular.module('castaLabControllers', ['ngSanitize']);

castaLabControllers.controller('MainContoller', ['$scope', '$log', '$http',
	function($scope, $log, $http) {

		console.log('Estamos en MainController');
		
	    

	}]);

castaLabControllers.controller('LastPostController', ['$scope', '$log', '$http', 'LastPostService', 
	function($scope, $log, $http, LastPostService) {

		console.log('Estamos en LastPostController');

		$scope.mostrarMensajeOK = false;
        $scope.mostrarMensajeERROR = false;		

		$http.get('/api/lastpost').success(function(data) {
			$scope.post = data[0].lastPost.posts[0];
		})
		.error(function(data) {
	        console.log('Error: ' + data);
	        $scope.mostrarMensajeERROR = true;	
	    });
	    
	    $scope.refreshLastPost = function() {

	    	console.log("Refrescando el ultimo articulo del blog");
	    	LastPostService.get({}, function(response) {

	    		console.log("tratando la respuesta: " + JSON.stringify(response));
	    		$scope.post = response.posts[0];

	    		//actualizamos en el proxy
	    		$http.put('/api/lastpost', response).
			    success(function(data, status, headers, config) {
			        // this callback will be called asynchronously
			        // when the response is available
			        console.log("Se ha enviado el nuevo post al proxy " + data);
			        $scope.mostrarMensajeOK = true;
			        $scope.mostrarMensajeERROR = false;
			      }).
			      error(function(data, status, headers, config) {
			        console.log("Errores enviado la notificacion: " + data);
			        $scope.mostrarMensajeERROR = true;
			        $scope.mostrarMensajeOK = false;
			      });


	    	});


	    };

	}]);
    
castaLabControllers.controller('LastMapController', ['$scope', '$log', '$http', 'MapsService',
	function($scope, $log, $http, MapsService) {

		console.log('Estamos en LastMapController');
		
		$http.get('/api/maps').success(function(data) {
			$scope.data = data;
		})
		.error(function(data) {
	        console.log('Error: ' + data);
	    });

		$scope.refreshMaps = function() {

	    	console.log("Refrescando los mapas");
	    	MapsService.get({}, function(response) {

	    		console.log("tratando la respuesta: " + JSON.stringify(response));
	    		
	    		$scope.data = response;

	    		//actualizamos en el proxy
	    		$http.put('/api/maps', response).
			    success(function(data, status, headers, config) {
			        // this callback will be called asynchronously
			        // when the response is available
			        console.log("Se han enviado los nuevos mapas al proxy " + data);
			        $scope.mostrarMensajeOK = true;
			        $scope.mostrarMensajeERROR = false;
			      }).
			      error(function(data, status, headers, config) {
			        console.log("Errores enviado los nuevos mapas: " + data);
			        $scope.mostrarMensajeERROR = true;
			        $scope.mostrarMensajeOK = false;
			      });
				
	    	});


	    };
	    

	}]);
    
castaLabControllers.controller('GCMUsersController', ['$scope', '$log', '$http',
	function($scope, $log, $http) {

		console.log('Estamos en GCMUsersController');
		
		$http.get('/api/gcmusers').success(function(data) {
			$scope.users = data;
		})
		.error(function(data) {
	        console.log('Error: ' + data);
	    });
		
	    

	}]);
    
    
castaLabControllers.controller('SendPushNotificationController', ['$scope', '$log', '$http',
	function($scope, $log, $http) {

		console.log('Estamos en SendPushNotificationController');

		$scope.mensaje = "";
		$scope.resultadoEnvio = "";
		$scope.mostrarResultado = false;

		$scope.send = function() {

			console.log("Enviando notificacion al servidor GCM");

			var mensaje = JSON.stringify({"mensaje" : $scope.mensaje});
		    
		    $http.post('/api/sendpushnotification', mensaje).
		    success(function(data, status, headers, config) {
		        // this callback will be called asynchronously
		        // when the response is available
		        console.log("Se ha enviado la notificacion correctamente " + data);
		        $scope.resultadoEnvio = data;
		        $scope.mostrarResultado = true;
		      }).
		      error(function(data, status, headers, config) {
		        console.log("Errores enviado la notificacion: " + data);
		      });

		};
		
	    

	}]);

castaLabControllers.controller('LoginController', ['$scope', '$location', 'AuthService',
	function ($scope, $location, AuthService) {

	    console.log(AuthService.getUserStatus());

	    $scope.login = function () {

		      // initial values
		      $scope.error = false;
		      $scope.disabled = true;

		      // call login from service
		      AuthService.login($scope.loginForm.username, $scope.loginForm.password)
		        // handle success
		        .then(function () {
		          $location.path('/public/lastmap');
		          $scope.disabled = false;
		          $scope.loginForm = {};
		        })
		        // handle error
		        .catch(function () {
		          $scope.error = true;
		          $scope.errorMessage = "Invalid username and/or password";
		          $scope.disabled = false;
		          $scope.loginForm = {};
		        });

	    };

}]);

castaLabControllers.controller('LogoutController', ['$scope', '$location', 'AuthService',
	function ($scope, $location, AuthService) {

	    $scope.logout = function () {

	      console.log(AuthService.getUserStatus());

	      // call logout from service
	      AuthService.logout()
	        .then(function () {
	          $location.path('/public/login');
	        });

	    };

}]);
    