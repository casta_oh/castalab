var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var UserDetail = mongoose.Schema({
	username: String,
	password: String
    }, {
      collection: 'userInfo'
    });

// methods ======================
// generating a hash
UserDetail.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
UserDetail.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('userInfo', UserDetail);